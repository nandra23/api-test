import unittest
import requests
import json
import datetime

from pprint import pprint

class Test_API(unittest.TestCase):
	
	API_URL = 'https://www.reqres.in'

	def test_list(self):
		print('[] ======Starting test_list=====')
		response = requests.get(self.API_URL + '/api/users?page=2')

		#print ('\033[1;37mciao!')

		#eve = response.json()['data'][0]
		#pprint(eve)
		#print(eve['first_name']+''+eve['last_name'])		

		#print(  response.json()['data'][0]['first_name']  )
		#print(response.status_code)
		#print(response.reason)
		#pprint(response.json())
		#return

		#self.assertEqual(len(response.json()['data']),3)
		self.assertEqual(response.json()['data'][0]['id'], 4)
		self.assertEqual(response.json()['data'][0]['first_name'], 'Eve')
		self.assertEqual(response.json()['data'][0]['last_name'], 'Holt')
	
	
	def test_single_user(self):
		print('[] ======Starting test_single_user=====')
		response = requests.get(self.API_URL + '/api/users/2')

		self.assertEqual(response.json()['data']['id'], 2)
		self.assertEqual(response.json()['data']['first_name'], 'Janet')
		self.assertEqual(response.json()['data']['last_name'], 'Weaver')


	def test_four_oh_four(self):
		print('[] ======Starting test_four_oh_four=====')
		response = requests.get(self.API_URL + '/api/users/23')

		self.assertEqual(response.status_code, 404)
		self.assertEqual(response.json(), {})


	def test_create_user(self):
		print('[] ======Starting test_create_user=====')
	
		data = {
   			"name": "morpheus",
    		        "job": "leader"
			}
		response = requests.post(self.API_URL + '/api/users', data)
		
		self.assertEqual(response.status_code, 201)
		print('[] User created with id: '+ response.json()['id'])
		self.assertEqual(response.json()['name'], data['name'] )

		date_now = datetime.datetime.now()
		date_cool  =date_now.strftime('%Y-%m-%d')		
		#date_cool2  =date_now.strftime('%y-%m-%d')		
		#date_cool3  =date_now.strftime('%y-%M-%D')		
		print(date_cool)
		#print(date_cool2)
		#print(date_cool3)

		status = False
		if date_cool in response.json()['createdAt']:
			status = True
		print('[-] User created with id: ' + str(response.json()['id'] + 'at' + date_cool))
		self.assertTrue(status)


	def test_update(self):
		print('[]=====Starting test_update======')


	
	#def test_




if __name__ == '__main__':
	unittest.main()

